package com.olympus.crypto.hash;

import com.olympus.constants.Constants;
import com.olympus.error.exceptions.FactoryTypeException;

public class HashCipherFactory {

	public static HashCipher getInstance(String type) {
		if(type.equalsIgnoreCase(Constants.sha384)) {
			return new SHACrypto("SHA-384");
		}
		throw new FactoryTypeException("Hash cipher type not supported or value is not correct");
	}
}
