package com.olympus.crypto.hash;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SHACrypto implements HashCipher {	
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String hashType = "SHA-256";
	
	public SHACrypto(String hashType) {
		logger.info("SHACrypto()#hashType = {}", hashType);
		this.hashType = hashType;
	}
	
	@Override
	public String hash(String input, String charset) {
		logger.info("hash()#input = {}", input);
		logger.info("hash()#charset = {}", charset);
		
		String hashedData = "";
		try { 
            MessageDigest md = MessageDigest.getInstance(hashType); 
            byte[] messageDigest = md.digest(input.getBytes(charset)); 
            hashedData = Hex.encodeHexString(messageDigest);
            return hashedData;
        } 
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) { 
        	logger.error("hash() - Error while hashing [{}]", e.getMessage());
        }
		return hashedData;
	}
}
