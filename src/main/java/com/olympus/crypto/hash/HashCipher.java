package com.olympus.crypto.hash;

public interface HashCipher {
	String hash(String input, String charset);
}
