package com.olympus.crypto.kcnfg;

import com.olympus.model.KConfigurationDataRequest;

public interface KSeriesGenerator {
	String genEncryptedConfigurationData(KConfigurationDataRequest kConfDataRequest);
}
