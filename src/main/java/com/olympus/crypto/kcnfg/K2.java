package com.olympus.crypto.kcnfg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.manager.CipherManager;
import com.olympus.crypto.cipher.manager.CipherManagerFactory;
import com.olympus.crypto.cipher.symm.SymmetricCipher;
import com.olympus.crypto.cipher.symm.SymmetricCipherFactory;
import com.olympus.model.ApplicationBySubs;
import com.olympus.model.CipherParamRequest;
import com.olympus.model.CnfgParams;
import com.olympus.model.KConfigurationDataRequest;
import com.olympus.repository.FirebaseConfigurationDataRepository;

@Component
public class K2 implements KSeriesGenerator{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private FirebaseConfigurationDataRepository firebaseConfigurationDataRepository;
	
	private ApplicationBySubs applicationBySubs;
	private CnfgParams cnfgParams;
	
	@Autowired
	public K2(FirebaseConfigurationDataRepository firebaseConfigurationDataRepository) {
		this.firebaseConfigurationDataRepository = firebaseConfigurationDataRepository;
	}
	
	@Override
	public String genEncryptedConfigurationData(KConfigurationDataRequest kConfDataRequest) {
		
		applicationBySubs = firebaseConfigurationDataRepository.getApplicationBySubsData(kConfDataRequest.getObjectId());
		cnfgParams = firebaseConfigurationDataRepository.getConfigurationData("cnfg");
		CipherParamRequest cipherParamRequest = kConfDataRequest.getCipher();

		String hTPKSys = cnfgParams.getHtpkSys();		

		String k2Value = buildReadDataConfigK2(applicationBySubs, kConfDataRequest);
		logger.debug("keyGenMethod()#k2Value : {}", k2Value);

		SymmetricCipher aes256 = SymmetricCipherFactory.
				getInstance(cipherParamRequest.getAlgorithm(), hTPKSys, cipherParamRequest.getCharset());	

		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);

		String k2 = symKeyManager.encryptProtectedKey(k2Value, aes256);
		logger.debug("keyGenMethod()#k2 : {}", k2);
		return k2;
	}
	
	private String buildReadDataConfigK2(ApplicationBySubs applicationBySubs, KConfigurationDataRequest kConfDataRequest) {
		ReadCnfDataK readCnfK2 = new ReadCnfDataK();
		readCnfK2.setEntityId(kConfDataRequest.getObjectId());
		readCnfK2.setWorkingEntityDerivedKey(applicationBySubs.getWorkingDerivativeKey());
		readCnfK2.setServicePort(applicationBySubs.getServicePort());
		readCnfK2.setEntityEndPoint(applicationBySubs.getApplicationEndpoint());

		ObjectMapper objectMapper = new ObjectMapper();
		String k1Value = "";
		try {
			k1Value = objectMapper.writeValueAsString(readCnfK2);
		} catch (JsonProcessingException e) {
			logger.error("buildReadDataConfigK2() - Error while mapping object to json. [{}]", e.getMessage());
		}
		return k1Value;
	}

}
