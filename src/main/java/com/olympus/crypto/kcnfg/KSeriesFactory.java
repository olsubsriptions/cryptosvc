package com.olympus.crypto.kcnfg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olympus.error.exceptions.FactoryTypeException;

@Component
public class KSeriesFactory {
	
	private static K1 k1;
	private static K2 k2;
	
	@Autowired
	public KSeriesFactory(K1 k1, K2 k2){
		KSeriesFactory.k1 = k1;
		KSeriesFactory.k2 = k2;
	}
	
	public static KSeriesGenerator getType(String keyGenType) {
		if(keyGenType.equalsIgnoreCase("K1")) {
			return k1;
		}
		else if(keyGenType.equalsIgnoreCase("K2")) {
			return k2;
		}
		throw new FactoryTypeException("KSeries Type not supported or value is not correct");
	}
}
