package com.olympus.crypto.kcnfg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.manager.CipherManager;
import com.olympus.crypto.cipher.manager.CipherManagerFactory;
import com.olympus.crypto.cipher.symm.SymmetricCipher;
import com.olympus.crypto.cipher.symm.SymmetricCipherFactory;
import com.olympus.model.CipherParamRequest;
import com.olympus.model.CnfgParams;
import com.olympus.model.KConfigurationDataRequest;
import com.olympus.repository.FirebaseConfigurationDataRepository;

@Component
public class K1 implements KSeriesGenerator{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private FirebaseConfigurationDataRepository firebaseConfigurationDataRepository;
	
	private CnfgParams cnfgParams;
	
	@Autowired
	public K1(FirebaseConfigurationDataRepository firebaseConfigurationDataRepository) {
		this.firebaseConfigurationDataRepository = firebaseConfigurationDataRepository;
	}

	@Override
	public String genEncryptedConfigurationData(KConfigurationDataRequest kConfDataRequest) {
		
		cnfgParams = firebaseConfigurationDataRepository.getConfigurationData("cnfg");
		
		CipherParamRequest cipherParamRequest = kConfDataRequest.getCipher();

		String hTPKSys = cnfgParams.getHtpkSys();		

		String k1Value = buildReadDataConfigK1(cnfgParams, kConfDataRequest);
		logger.debug("keyGenMethod()#k1Value : {}", k1Value);

		SymmetricCipher aes256 = SymmetricCipherFactory.
				getInstance(cipherParamRequest.getAlgorithm(), hTPKSys, cipherParamRequest.getCharset());	

		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);

		String k1 = symKeyManager.encryptProtectedKey(k1Value, aes256);
		logger.debug("keyGenMethod()#k1 : {}", k1);
		return k1;
	}

	private String buildReadDataConfigK1(CnfgParams cnfgParams, KConfigurationDataRequest kConfDataRequest) {
		ReadCnfDataK readCnfK1 = new ReadCnfDataK();
		readCnfK1.setEntityId(kConfDataRequest.getObjectId());
		readCnfK1.setWorkingEntityDerivedKey(kConfDataRequest.getDerivativeKey());
		readCnfK1.setServicePort(cnfgParams.getServicePort());
		readCnfK1.setEntityEndPoint(cnfgParams.getSubscriptionEndpoint());

		ObjectMapper objectMapper = new ObjectMapper();
		String k1Value = "";
		try {
			k1Value = objectMapper.writeValueAsString(readCnfK1);
		} catch (JsonProcessingException e) {
			logger.error("buildReadDataConfigK1() - Error while mapping object to json. [{}]", e.getMessage());
		}
		return k1Value;
	}
}
