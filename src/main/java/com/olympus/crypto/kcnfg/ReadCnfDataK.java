package com.olympus.crypto.kcnfg;

public class ReadCnfDataK {
	private String workingEntityDerivedKey;
	private String entityId;
	private String entityEndPoint;
	private String servicePort;
	
	public String getServicePort() {
		return servicePort;
	}
	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}
	public String getWorkingEntityDerivedKey() {
		return workingEntityDerivedKey;
	}
	public void setWorkingEntityDerivedKey(String workingEntityDerivedKey) {
		this.workingEntityDerivedKey = workingEntityDerivedKey;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getEntityEndPoint() {
		return entityEndPoint;
	}
	public void setEntityEndPoint(String entityEndPoint) {
		this.entityEndPoint = entityEndPoint;
	}
}
