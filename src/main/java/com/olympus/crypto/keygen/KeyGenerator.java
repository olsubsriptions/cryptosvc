package com.olympus.crypto.keygen;

import com.olympus.model.CnfgParams;
import com.olympus.model.KeyGeneratorRequest;

public interface KeyGenerator {
	String keyGenMethod(CnfgParams cnfgParams, KeyGeneratorRequest keyGeneratorRequest);
}
