package com.olympus.crypto.keygen;

import com.olympus.error.exceptions.FactoryTypeException;

public class KeyGeneratorFactory {
	
	public static KeyGenerator getType(String keyGenType) {
		if(keyGenType.equalsIgnoreCase("SK")) {
			return new SecurityKey();
		}
		throw new FactoryTypeException("KeyGenerator type not supported or value is not correct");
	}
}
