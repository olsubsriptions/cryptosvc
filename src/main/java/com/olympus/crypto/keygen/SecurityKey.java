package com.olympus.crypto.keygen;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.manager.CipherManager;
import com.olympus.crypto.cipher.manager.CipherManagerFactory;
import com.olympus.crypto.cipher.symm.SymmetricCipher;
import com.olympus.crypto.cipher.symm.SymmetricCipherFactory;
import com.olympus.crypto.hash.HashCipher;
import com.olympus.crypto.hash.HashCipherFactory;
import com.olympus.model.CipherParamRequest;
import com.olympus.model.CnfgParams;
import com.olympus.model.KeyGeneratorRequest;

public class SecurityKey implements KeyGenerator{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());	
	
	@Override
	public String keyGenMethod(CnfgParams cnfgParams, KeyGeneratorRequest keyGeneratorRequest) {
		CipherParamRequest cipherParamRequest = keyGeneratorRequest.getCipher();
		String hPKSys = cnfgParams.getHpkSys();
		
		HashCipher hashCipher = HashCipherFactory.getInstance(Constants.sha384);
		String protectedKey = hashCipher.hash(UUID.randomUUID().toString(), cipherParamRequest.getCharset());
		logger.debug("keyGenMethod()#protectedKey : {}", protectedKey);
		
		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
		SymmetricCipher aes256 = SymmetricCipherFactory.
				getInstance(cipherParamRequest.getAlgorithm(), hPKSys, cipherParamRequest.getCharset());		
		String securityKey = symKeyManager.encryptProtectedKey(protectedKey, aes256);
		logger.debug("keyGenMethod()#securityKey : {}", securityKey);
		return securityKey;
	}

}
