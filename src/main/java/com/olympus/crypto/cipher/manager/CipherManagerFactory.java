package com.olympus.crypto.cipher.manager;

import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.symm.SymmetricCipherManager;
import com.olympus.error.exceptions.FactoryTypeException;

public class CipherManagerFactory {

	public static CipherManager getInstance(String type) {
		if(type.equalsIgnoreCase(Constants.symmetric)) {
			return new SymmetricCipherManager();
		}
		throw new FactoryTypeException("Cipher type not supported or value is not correct");
	}
}
