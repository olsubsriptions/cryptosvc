package com.olympus.crypto.cipher.manager;

import com.olympus.crypto.cipher.symm.SymmetricCipher;

public interface CipherManager {
	String encryptProtectedKey(String protectedKey, SymmetricCipher symCipher);
	String decryptSecurityKey(String securityKey, SymmetricCipher symCipher);
}
