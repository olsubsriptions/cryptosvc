package com.olympus.crypto.cipher.symm;

public interface SymmetricCipher {
	
	void initCipher(String password, String charset);
	String encrypt(String valueToEncrypt);
	String decrypt(String valueToDecrypt);
}
