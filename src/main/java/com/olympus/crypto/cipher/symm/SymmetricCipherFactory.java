package com.olympus.crypto.cipher.symm;

import com.olympus.constants.Constants;
import com.olympus.error.exceptions.FactoryTypeException;

public class SymmetricCipherFactory {

	private static SymmetricCipher getSymCipherType(String algorithm) {
		if(algorithm.equals(Constants.aes256)) {
			return new AES256();
		}
		throw  new FactoryTypeException("Algorithm not supported or value is not correct");	
	}

	public static SymmetricCipher getInstance(String algorithm, String password, String charset) {
		SymmetricCipher symCipher = getSymCipherType(algorithm);
		symCipher.initCipher(password, charset);
		return symCipher;
	}
}
