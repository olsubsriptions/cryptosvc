package com.olympus.crypto.cipher.symm;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AES256 implements SymmetricCipher{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String secretKey;
	private byte[] iv;
	private byte[] salt;
	private String charset = "UTF-8";

	private final String transformation = "AES/CBC/PKCS5PADDING";

	@Override
	public void initCipher (String secretKey, String charset) {
		logger.info("initCipher()#secretKey : {}", secretKey);
		logger.info("initCipher()#charset : {}", charset);
		
		this.secretKey = secretKey;
		this.iv = generateIV();
		this.salt = generateSalt();
		this.charset = charset;
	}

	public byte[] generateSalt() {
//		SecureRandom random = new SecureRandom();
//		byte [] saltGen = random.generateSeed(8);
		byte [] saltGen = {-93, 17, 45, 78, -103, -82, 52, -24};
		return  saltGen;
	}

	public byte[] generateIV() {
		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		return iv;
	}

	@Override
	public String encrypt(String valueToEncrypt) {
		logger.info("encrypt()#valueToEncrypt : {}", valueToEncrypt);
		String hexEncryptedValue = "";
		try{
			IvParameterSpec ivspec = new IvParameterSpec(iv);;
			SecretKeySpec secretKeySpec = generateSecreKeyObject();
			Cipher cipher = Cipher.getInstance(transformation);
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
			byte[] encryptedValue = cipher.doFinal(valueToEncrypt.getBytes(charset));
			hexEncryptedValue = Hex.encodeHexString(encryptedValue);
			return hexEncryptedValue;
		}
		catch (Exception e){
			logger.error("encrypt() - Error while encrypting [{}]", e.getMessage());
		}
		return hexEncryptedValue;
	}

	@Override
	public String decrypt(String valueToDecrypt) {
		logger.info("decrypt()#valueToDecrypt : {}", valueToDecrypt);
		IvParameterSpec ivspec = new IvParameterSpec(iv);;
		SecretKeySpec secretKeySpec = generateSecreKeyObject();
		Cipher cipher;
		String decryptedValue = "";
		try {
			cipher = Cipher.getInstance(transformation);
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivspec);
			byte[] binaryDecryptedValue = Hex.decodeHex(valueToDecrypt);
			decryptedValue = new String(cipher.doFinal(binaryDecryptedValue));
			return decryptedValue;
			
		} catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException | 
				NoSuchPaddingException | DecoderException | IllegalBlockSizeException | BadPaddingException e) {
			logger.error("decrypt() - Error while decrypting [{}]", e.getMessage());
		}
		return decryptedValue;
	}

	private SecretKeySpec generateSecreKeyObject() {
		logger.info("generateSecreKeyObject()");
		SecretKeySpec secretKeySpec = null;
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt, 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.error("generateSecreKeyObject()# - Error while decrypting [{}]", e.getMessage());
		}
		return secretKeySpec;

	}

}
