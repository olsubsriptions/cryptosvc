package com.olympus.crypto.cipher.symm;

import com.olympus.crypto.cipher.manager.CipherManager;

public class SymmetricCipherManager implements CipherManager{
	
	@Override
	public String encryptProtectedKey(String protectedKey, SymmetricCipher symCipher) {
		String securityKey = symCipher.encrypt(protectedKey);
		return securityKey;
	}
	
	@Override
	public String decryptSecurityKey(String securityKey, SymmetricCipher symCipher) {
		String protectedKey = symCipher.decrypt(securityKey);
		return protectedKey;
	}
}
