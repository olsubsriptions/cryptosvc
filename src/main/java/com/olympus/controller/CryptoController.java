package com.olympus.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.olympus.model.CipherRequest;
import com.olympus.model.KeyGeneratorRequest;
import com.olympus.model.ResponseObject;
import com.olympus.service.CipherService;
import com.olympus.service.KeyGeneratorService;

@RestController
@RequestMapping("v1/crypto")
class CryptoController {

	private KeyGeneratorService keyGeneratorService;
	private CipherService cipherService;
	
	@Autowired
	public CryptoController(KeyGeneratorService keyGeneratorService,
			CipherService cipherService) {
		this.keyGeneratorService = keyGeneratorService;
		this.cipherService = cipherService;
	}

	@PostMapping
	@RequestMapping("keyGenerator")
	@ResponseStatus(HttpStatus.OK)
	public ResponseObject keyGenerator(@RequestBody @Valid KeyGeneratorRequest keyGeneratorRequest) {
		String encKey = keyGeneratorService.generateKey(keyGeneratorRequest);
		
		ResponseObject responseObject = new ResponseObject();
		responseObject.setEncKey(encKey);
		return responseObject;
	}
	
	@PostMapping ("encrypt")
	@ResponseStatus(HttpStatus.OK)
	public ResponseObject encrypt(@RequestBody @Valid CipherRequest cipherRequest) {
		
		List<String> encryptedValues = cipherService.encrypt(cipherRequest);
		
		ResponseObject responseObject = new ResponseObject();
		responseObject.setDataList(encryptedValues);
		return responseObject;
	}
	
	@PostMapping ("decrypt")
	@ResponseStatus(HttpStatus.OK)
	public ResponseObject decrypt(@RequestBody @Valid CipherRequest cipherRequest) {
		
		List<String> decryptedValues = cipherService.decrypt(cipherRequest);
		
		ResponseObject responseObject = new ResponseObject();
		responseObject.setDataList(decryptedValues);
		return responseObject;
	}	
}
