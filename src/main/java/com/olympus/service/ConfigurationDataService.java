package com.olympus.service;

import com.olympus.model.KConfigurationDataRequest;

public interface ConfigurationDataService {
	String generateConfigurationData(KConfigurationDataRequest kConfDataRequest);
}
