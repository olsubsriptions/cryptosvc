package com.olympus.service;

import java.util.List;

import com.olympus.model.CipherParamRequest;
import com.olympus.model.CipherRequest;

public interface CipherService {
	List<String> encrypt(CipherParamRequest cipher, String securityKey, List<String> dataList);
	List<String> decrypt(CipherParamRequest cipher, String securityKey, List<String> dataList);
	List<String> encrypt(CipherRequest cipherRequest);
	List<String> decrypt(CipherRequest cipherRequest);
}
