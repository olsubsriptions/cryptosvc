package com.olympus.service;

import com.olympus.model.KeyGeneratorRequest;

public interface KeyGeneratorService {
//	String generateSecurityKey(CipherParamRequest cipher);
	String generateKey(KeyGeneratorRequest keyGeneratorRequest);
//	String generateKSeriesKey(KSeriesGeneratorRequest kSeriesGeneratorRequest);
}
