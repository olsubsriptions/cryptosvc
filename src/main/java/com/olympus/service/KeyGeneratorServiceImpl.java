package com.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olympus.crypto.keygen.KeyGenerator;
import com.olympus.crypto.keygen.KeyGeneratorFactory;
import com.olympus.model.CnfgParams;
import com.olympus.model.KeyGeneratorRequest;
import com.olympus.repository.FirebaseConfigurationDataRepository;

@Service
public class KeyGeneratorServiceImpl implements KeyGeneratorService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private FirebaseConfigurationDataRepository configurationDataRepository;
	
	@Autowired
	public KeyGeneratorServiceImpl(FirebaseConfigurationDataRepository configurationDataRepository) {
		this.configurationDataRepository = configurationDataRepository;
	}
	
	@Override
	public String generateKey(KeyGeneratorRequest keyGeneratorRequest) {
		logger.info("generateKey( : [keyGeneratorRequest = {}])", keyGeneratorRequest);
		KeyGenerator keyGenerator = KeyGeneratorFactory.getType(keyGeneratorRequest.getKeyType());
		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
		String key = keyGenerator.keyGenMethod(cnfgParams, keyGeneratorRequest);
		return key;
	}
}
