package com.olympus.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.manager.CipherManager;
import com.olympus.crypto.cipher.manager.CipherManagerFactory;
import com.olympus.crypto.cipher.symm.SymmetricCipher;
import com.olympus.crypto.cipher.symm.SymmetricCipherFactory;
import com.olympus.model.CipherParamRequest;
import com.olympus.model.CipherRequest;
import com.olympus.model.CnfgParams;
import com.olympus.model.Subscription;
import com.olympus.repository.FirebaseConfigurationDataRepository;

@Service
public class CipherServiceImpl implements CipherService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
	private FirebaseConfigurationDataRepository configurationDataRepository;
	
	@Autowired
	public CipherServiceImpl(FirebaseConfigurationDataRepository configurationDataRepository) {
		this.configurationDataRepository = configurationDataRepository;
	}

	@Override
	public List<String> encrypt(CipherParamRequest cipher, String securityKey, List<String> dataList) {
		logger.info("encrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipher, dataList);
		
		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
		List<String> encryptedValuesList = new ArrayList<String>();		
		
		String charset = cipher.getCharset();
		String algorithm = cipher.getAlgorithm();
		
		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(algorithm, cnfgParams.getHpkSys(), charset);
		String protectedKey = symKeyManager.decryptSecurityKey(securityKey, pKeyAES);
		logger.debug("encrypt#protectedKey : {}", protectedKey);
		
		SymmetricCipher aes256 = SymmetricCipherFactory.getInstance(algorithm, protectedKey, charset);
		List<String> valuesToEncrypt = dataList; //Hex Value
		
		valuesToEncrypt.stream().forEach(valueToEncrypt -> {
			String encryptedValue = aes256.encrypt(valueToEncrypt);
			encryptedValuesList.add(encryptedValue);
		});

		logger.debug("encrypt#encryptedValuesList : {}", encryptedValuesList);
		
		return encryptedValuesList;
	}	

	@Override
	public List<String> decrypt(CipherParamRequest cipher, String securityKey, List<String> dataList) {
		logger.info("decrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipher, dataList);
		
		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
		List<String> decryptedValuesList = new ArrayList<String>();		
		String charset = cipher.getCharset();
		String algorithm = cipher.getAlgorithm();
		
		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(algorithm, cnfgParams.getHpkSys(), charset);
		String protectedKey = symKeyManager.decryptSecurityKey(securityKey, pKeyAES);
		logger.debug("decrypt#protectedKey : {}", protectedKey);
		
		SymmetricCipher aes256 = SymmetricCipherFactory.getInstance(algorithm, protectedKey, charset);		
		List<String> valuesToDecrypt = dataList;
		
		valuesToDecrypt.stream().forEach(valueToDecrypt -> {
			String decryptedValue = aes256.decrypt(valueToDecrypt);		
			decryptedValuesList.add(decryptedValue);
		});
		
		logger.debug("decrypt#decryptedValuesList : {}", decryptedValuesList);
		
		return decryptedValuesList;
	}	
	
	@Override
	public List<String> encrypt(CipherRequest cipherRequest) {
		
		logger.info("encrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipherRequest.getCipher(), cipherRequest.getDataList());
		String indexKey = cipherRequest.getIndexKey();
		CipherParamRequest cipher = cipherRequest.getCipher();
		List<String> dataList = cipherRequest.getDataList();
		
		List<String> encryptedValuesList = new ArrayList<String>();
		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
		
		SymmetricCipher aes256;		
		
		if(indexKey == null) {
			
			aes256 = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), cnfgParams.getHtpkSys(), cipher.getCharset());
			
		} else {
			
			Subscription subscription = configurationDataRepository.getSubscriptionData(cipherRequest.getIndexKey());
			CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
			SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), cnfgParams.getHpkSys(), cipher.getCharset());
			String protectedKey = symKeyManager.decryptSecurityKey(subscription.getSecurityKey(), pKeyAES);
			logger.debug("encrypt#protectedKey : {}", protectedKey);
			
			aes256 = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), protectedKey, cipher.getCharset());
			
		}	
		
		dataList.stream().forEach(data -> {
			String encryptedValue = aes256.encrypt(data);
			encryptedValuesList.add(encryptedValue);
		});
			
		
		logger.debug("encrypt#encryptedValuesList : {}", encryptedValuesList);
		return encryptedValuesList;
	}

	@Override
	public List<String> decrypt(CipherRequest cipherRequest) {
		
		logger.info("decrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipherRequest.getCipher(), cipherRequest.getDataList());
		String indexKey = cipherRequest.getIndexKey();
		CipherParamRequest cipher = cipherRequest.getCipher();
		List<String> dataList = cipherRequest.getDataList();
		
		List<String> decryptedValuesList = new ArrayList<String>();
		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
		
		SymmetricCipher aes256;
		
		if(indexKey == null) {
			
			aes256 = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), cnfgParams.getHtpkSys(), cipher.getCharset());
			
		} else {
			
			Subscription subscription = configurationDataRepository.getSubscriptionData(cipherRequest.getIndexKey());
			CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
			SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), cnfgParams.getHpkSys(), cipher.getCharset());
			String protectedKey = symKeyManager.decryptSecurityKey(subscription.getSecurityKey(), pKeyAES);
			logger.debug("decrypt#protectedKey : {}", protectedKey);
			
			aes256 = SymmetricCipherFactory.getInstance(cipher.getAlgorithm(), protectedKey, cipher.getCharset());
			
		}
		
		dataList.stream().forEach(data -> {
			String encryptedValue = aes256.decrypt(data);
			decryptedValuesList.add(encryptedValue);
		});
		
		logger.debug("decrypt#decryptedValuesList : {}", decryptedValuesList);		
		return decryptedValuesList;
	}
	
//	@Override
//	public List<String> encrypt(CipherParamRequest cipher, String securityKey, List<String> dataList) {
//		logger.info("encrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipher, dataList);
//		
//		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
//		List<String> encryptedValuesList = new ArrayList<String>();		
//		
//		String charset = cipher.getCharset();
//		String algorithm = cipher.getAlgorithm();
//		
//		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
//		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(algorithm, cnfgParams.getHpkSys(), charset);
//		String protectedKey = symKeyManager.decryptSecurityKey(securityKey, pKeyAES);
//		logger.debug("encrypt#protectedKey : {}", protectedKey);
//		
//		SymmetricCipher aes256 = SymmetricCipherFactory.getInstance(algorithm, protectedKey, charset);
//		List<String> valuesToEncrypt = dataList; //Hex Value
//		
//		valuesToEncrypt.stream().forEach(valueToEncrypt -> {
//			String encryptedValue = aes256.encrypt(valueToEncrypt);
//			encryptedValuesList.add(encryptedValue);
//		});
//
//		logger.debug("encrypt#encryptedValuesList : {}", encryptedValuesList);
//		
//		return encryptedValuesList;
//	}
//
//	@Override
//	public List<String> decrypt(CipherParamRequest cipher, String securityKey, List<String> dataList) {
//		logger.info("decrypt() : [cipher={}], [securityKey={}], [dataList={}]", cipher, dataList);
//		
//		CnfgParams cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
//		List<String> decryptedValuesList = new ArrayList<String>();		
//		String charset = cipher.getCharset();
//		String algorithm = cipher.getAlgorithm();
//		
//		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
//		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance(algorithm, cnfgParams.getHpkSys(), charset);
//		String protectedKey = symKeyManager.decryptSecurityKey(securityKey, pKeyAES);
//		logger.debug("decrypt#protectedKey : {}", protectedKey);
//		
//		SymmetricCipher aes256 = SymmetricCipherFactory.getInstance(algorithm, protectedKey, charset);		
//		List<String> valuesToDecrypt = dataList;
//		
//		valuesToDecrypt.stream().forEach(valueToDecrypt -> {
//			String decryptedValue = aes256.decrypt(valueToDecrypt);		
//			decryptedValuesList.add(decryptedValue);
//		});
//		
//		logger.debug("decrypt#decryptedValuesList : {}", decryptedValuesList);
//		
//		return decryptedValuesList;
//	}
}