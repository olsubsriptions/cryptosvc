package com.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olympus.crypto.kcnfg.KSeriesFactory;
import com.olympus.crypto.kcnfg.KSeriesGenerator;
import com.olympus.model.KConfigurationDataRequest;

@Service
public class ConfigurationDataServiceImpl implements ConfigurationDataService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public ConfigurationDataServiceImpl() {
	}
	
	@Override
	public String generateConfigurationData(KConfigurationDataRequest kConfDataRequest) {
		logger.info("generateKey( : [kConfDataRequest = {}])", kConfDataRequest);
		KSeriesGenerator kSeriesGenerator = KSeriesFactory.getType(kConfDataRequest.getKeyType());
		String kDataEncrypted = kSeriesGenerator.genEncryptedConfigurationData(kConfDataRequest);
		return kDataEncrypted;
	}
}
