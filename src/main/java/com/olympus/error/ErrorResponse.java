package com.olympus.error;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ErrorResponse {

	@JsonProperty("status")
	private HttpStatus status;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("debugMessage")
	private String debugMessage;
	
	@JsonProperty("subErrors")
	private List<SubError> subErrors;

	private ErrorResponse() {
		this.timestamp = LocalDateTime.now();
	}

	ErrorResponse(HttpStatus status) {
		this();
		this.status = status;
	}

	ErrorResponse(HttpStatus status, String message, Throwable ex) {
		this();
		this.status = status;
		this.message = message;
		this.debugMessage = ex.getLocalizedMessage();
	}
	
	public void addValidationErrors(List<FieldError> fieldErrorList) {
		fieldErrorList.stream().forEach( fieldError -> {
			addError(() -> fieldError);
		});
	}
	
	private void addError(Supplier<FieldError> fieldError) {
		addSubError(new RequestValidationError(
				fieldError.get().getField(), 
				fieldError.get().getDefaultMessage()));
	}
	
	private void addSubError(SubError subError) {
		if(subErrors == null)
			subErrors = new ArrayList<SubError>();
		subErrors.add(subError);
	}
}
