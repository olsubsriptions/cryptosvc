package com.olympus.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestValidationError implements SubError{
	@JsonProperty("field")
    private String field;
	
	@JsonProperty("message")
    private String message;
    
    RequestValidationError(String field, String message) {
        this.field = field;
        this.message = message;
    }
}
