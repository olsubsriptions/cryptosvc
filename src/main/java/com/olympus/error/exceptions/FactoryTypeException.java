package com.olympus.error.exceptions;

public class FactoryTypeException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public FactoryTypeException(String message) {
		super(message);
	}
}
