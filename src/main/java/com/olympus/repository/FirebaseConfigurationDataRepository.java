package com.olympus.repository;

import com.olympus.model.ApplicationBySubs;
import com.olympus.model.CnfgParams;
import com.olympus.model.Subscription;

public interface FirebaseConfigurationDataRepository {
	CnfgParams getConfigurationData(String documentName);
	ApplicationBySubs getApplicationBySubsData(String documentName);
	Subscription getSubscriptionData(String indexKey);
}
