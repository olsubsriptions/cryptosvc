package com.olympus.repository;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.olympus.constants.Constants;
import com.olympus.crypto.hash.HashCipher;
import com.olympus.crypto.hash.HashCipherFactory;
import com.olympus.model.ApplicationBySubs;
import com.olympus.model.CnfgParams;
import com.olympus.model.Subscription;

@Repository
public class FirebaseConfigurationDataRepositoryImpl implements FirebaseConfigurationDataRepository{
	
	private Firestore getFirebaseClient;
	
	@Autowired
	public FirebaseConfigurationDataRepositoryImpl(Firestore getFirebaseClient) {
		this.getFirebaseClient = getFirebaseClient;
	}
	
	@Override
	public CnfgParams getConfigurationData(String documentName){
		CnfgParams cnfgParams = new CnfgParams();
		HashCipher sha384Cipher = HashCipherFactory.getInstance(Constants.sha384);
		DocumentReference docRef = getFirebaseClient.collection("cnfg").document("cnfg01");
		ApiFuture<DocumentSnapshot> future = docRef.get();
		DocumentSnapshot document = null;
		try {
			document = future.get();
			String hPKSys = sha384Cipher.hash((String) document.getData().get(Constants.pksys), "UTF-8");
			String hTPKSys = sha384Cipher.hash((String) document.getData().get(Constants.tpksys), "UTF-8");
			cnfgParams.setHpkSys(hPKSys);
			cnfgParams.setHtpkSys(hTPKSys);
			cnfgParams.setSubscriptionEndpoint((String) document.getData().get(Constants.entityEndpoint));
			cnfgParams.setServicePort((String) document.getData().get(Constants.servicePort));
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return cnfgParams;
	}

	@Override
	public ApplicationBySubs getApplicationBySubsData(String documentName) {
		ApplicationBySubs application = new ApplicationBySubs();
		DocumentReference docRefApp = getFirebaseClient.collection("w0790").document(documentName);
		
		ApiFuture<DocumentSnapshot> apiFutureApp = docRefApp.get();
		DocumentSnapshot document = null;
		try {
			document = apiFutureApp.get();	
			application.setWorkingDerivativeKey((String) document.getData().get("w0796"));
			application.setServicePort((String) document.getData().get("w0798"));
			application.setApplicationEndpoint((String) document.getData().get("w0795"));
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return application;
	}

	@Override
	public Subscription getSubscriptionData(String indexKey) {
		Subscription subscription = new Subscription();
		try {
			
			QuerySnapshot querySnapShotS = getFirebaseClient.collection("b4320")
				.whereEqualTo("b4325", indexKey)
				.get().get();
			
			if(!querySnapShotS.isEmpty()) {
				List<QueryDocumentSnapshot> listSubscriptionDocuments = querySnapShotS.getDocuments();
				subscription.setSubscriptionId(listSubscriptionDocuments.get(0).getId());
				subscription.setIndexKey(listSubscriptionDocuments.get(0).getString("b4325"));
				subscription.setSecurityKey(listSubscriptionDocuments.get(0).getString("b4322"));
			}
		
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return subscription;
	}
}
