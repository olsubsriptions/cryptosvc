package com.olympus.constants;

public class Constants {
	//Cnfg Field Values
	public static final String entityEndpoint = "subscriptionEndpoint";
	public static final String servicePort = "servicePort";
	public static final String tpksys = "tpksys";
	public static final String pksys = "pksys";
	
	// Algorithms
	public static final String aes256 ="AES256";
	public static final String symmetric = "SYM";
	public static final String sha384 = "SHA-384";
	
	
}
