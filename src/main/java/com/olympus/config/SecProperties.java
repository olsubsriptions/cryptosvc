package com.olympus.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("sec")
public class SecProperties {
	
	private String pksys;
	private String tpksys;
	
	private String endpoint;
	private String port;

	public String getPksys() {
		return pksys;
	}

	public void setPksys(String pksys) {
		this.pksys = pksys;
	}

	public String getTpksys() {
		return tpksys;
	}

	public void setTpksys(String tpksys) {
		this.tpksys = tpksys;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
}
