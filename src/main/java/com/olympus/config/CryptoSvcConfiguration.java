package com.olympus.config;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;

@Configuration
public class CryptoSvcConfiguration {

	@Bean
	public Firestore getFirebaseClient() throws IOException {
		FirestoreOptions firestoreOptions =
				FirestoreOptions.getDefaultInstance().toBuilder()
				.setCredentials(GoogleCredentials.getApplicationDefault())
				.setProjectId("subcriptions-2020-02")
				.build();
		Firestore fireStore = firestoreOptions.getService();
		return fireStore;	
	}
}
