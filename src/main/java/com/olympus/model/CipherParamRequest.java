package com.olympus.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CipherParamRequest {
	
	@NotNull(message = "Please provide an algorithm")
	@NotEmpty(message = "Algorithm can't be empty")
	private String algorithm;
	
	@NotNull(message = "Please provide an charset")
	@NotEmpty(message = "Charset can't be empty")
	private String charset;

	@JsonProperty("algorithm")
	public String getAlgorithm() {
		return algorithm;
	}
	
	@JsonProperty("charset")
	public String getCharset() {
		if(charset == null || charset.isEmpty())
			charset = "UTF-8";
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	@Override
	public String toString() {
		return "CipherParamRequest [algorithm=" + algorithm + ", charset=" + charset + "]";
	}
}
