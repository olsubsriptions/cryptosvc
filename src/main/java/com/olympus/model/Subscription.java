package com.olympus.model;

public class Subscription {
	
	private String subscriptionId;
	private String securityKey;
	private String indexKey;
	
	public String getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public String getSecurityKey() {
		return securityKey;
	}
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	public String getIndexKey() {
		return indexKey;
	}
	public void setIndexKey(String indexKey) {
		this.indexKey = indexKey;
	}
	
	@Override
	public String toString() {
		return "Subscription [subscriptionId=" + subscriptionId + ", securityKey=" + securityKey + ", indexKey="
				+ indexKey + "]";
	}	
}

