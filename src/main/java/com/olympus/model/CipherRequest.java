package com.olympus.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CipherRequest {
	
	@NotNull(message = "Please provide a dataList")
	@NotEmpty(message = "DataList can't be empty")
	private List<String> dataList;
	
	private String indexKey;
	
	@NotNull(message = "Please provide a cipher")
	@Valid
	private CipherParamRequest cipher;

	@JsonProperty("cipher")
	public CipherParamRequest getCipher() {
		return cipher;
	}

	@JsonProperty("cipher")
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}

	@JsonProperty("dataList")
	public List<String> getDataList() {
		return dataList;
	}

	@JsonProperty("dataList")
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}

	@JsonProperty("indexKey")
	public String getIndexKey() {
		return indexKey;
	}

	@JsonProperty("indexKey")
	public void setIndexKey(String indexKey) {
		this.indexKey = indexKey;
	}

	@Override
	public String toString() {
		return "CipherRequest [dataList=" + dataList + ", indexKey=" + indexKey + ", cipher=" + cipher + "]";
	}	
}
