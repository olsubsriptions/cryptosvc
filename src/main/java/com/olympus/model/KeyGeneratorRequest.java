package com.olympus.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KeyGeneratorRequest {

	@NotNull(message = "Please provide a keyType")
	@NotEmpty(message = "KeyType can't be empty")
	@Valid
	private String keyType;
	
//	private String objectId;
//	
//	private String derivativeKey;
	
	@NotNull(message = "Please provide a cipher")
	@Valid
	private CipherParamRequest cipher;

	@JsonProperty("cipher")
	public CipherParamRequest getCipher() {
		return cipher;
	}
	
	@JsonProperty("cipher")
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}

	@JsonProperty("keyType")
	public String getKeyType() {
		return keyType;
	}

	@JsonProperty("keyType")
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}

	@Override
	public String toString() {
		return "KeyGeneratorRequest [keyType=" + keyType + ", cipher=" + cipher + "]";
	}
	
//	@JsonProperty("objectId")
//	public String getObjectId() {
//		return objectId;
//	}
//
//	@JsonProperty("objectId")
//	public void setObjectId(String objectId) {
//		this.objectId = objectId;
//	}
//
//	@JsonProperty("derivativeKey")
//	public String getDerivativeKey() {
//		return derivativeKey;
//	}
//
//	@JsonProperty("derivativeKey")
//	public void setDerivativeKey(String derivativeKey) {
//		this.derivativeKey = derivativeKey;
//	}

}
