package com.olympus.model;

public class CnfgParams {
	
	private String hpkSys;
	private String servicePort;
	private String subscriptionEndpoint;
	private String htpkSys;
	
	public String getServicePort() {
		return servicePort;
	}
	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}
	public String getSubscriptionEndpoint() {
		return subscriptionEndpoint;
	}
	public void setSubscriptionEndpoint(String subscriptionEndpoint) {
		this.subscriptionEndpoint = subscriptionEndpoint;
	}
	public String getHpkSys() {
		return hpkSys;
	}
	public void setHpkSys(String hpkSys) {
		this.hpkSys = hpkSys;
	}
	public String getHtpkSys() {
		return htpkSys;
	}
	public void setHtpkSys(String htpkSys) {
		this.htpkSys = htpkSys;
	}
}
