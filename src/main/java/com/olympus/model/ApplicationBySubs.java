package com.olympus.model;

public class ApplicationBySubs {
	private String workingDerivativeKey;
	private String servicePort;
	private String applicationEndpoint;
	
	public String getWorkingDerivativeKey() {
		return workingDerivativeKey;
	}
	public void setWorkingDerivativeKey(String workingDerivativeKey) {
		this.workingDerivativeKey = workingDerivativeKey;
	}
	public String getServicePort() {
		return servicePort;
	}
	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}
	public String getApplicationEndpoint() {
		return applicationEndpoint;
	}
	public void setApplicationEndpoint(String applicationEndpoint) {
		this.applicationEndpoint = applicationEndpoint;
	}
}
