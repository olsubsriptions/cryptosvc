package com.olympus.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonPropertyOrder({ "dataList", "securityKey", "algorithm" })
public class CryptoRequest {
	
	@NotNull(message = "Please provide a dataList")
	@NotEmpty(message = "DataList can't be empty")
	private List<String> dataList;
	
	@NotNull(message = "Please provide a securityKey")
	@NotEmpty(message = "securityKey can't be empty")
	private String securityKey;

	@NotNull(message = "Please provide a cipher")
	@Valid
	private CipherParamRequest cipher;

	@JsonProperty("cipher")
	public CipherParamRequest getCipher() {
		return cipher;
	}
	
	@JsonProperty("cipher")
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}
	
	@JsonProperty("dataList")
	public List<String> getDataList() {
		if(dataList == null)
			dataList = new ArrayList<String>();
		return dataList;
	}
	
	@JsonProperty("securityKey")
	public String getSecurityKey() {
		return securityKey;
	}
	
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}

	@Override
	public String toString() {
		return "CryptoRequest [dataList=" + dataList + ", securityKey=" + securityKey + ", cipherParamRequest="
				+ cipher + "]";
	}
	
}
