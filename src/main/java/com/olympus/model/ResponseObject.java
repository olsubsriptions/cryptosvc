package com.olympus.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseObject {
	
	@JsonInclude(Include.NON_NULL)
	private List<String> dataList;
	
	@JsonInclude(Include.NON_NULL)
	private String encKey;
	
	@JsonInclude(Include.NON_NULL)
	private String encKCnfgData;
	
	@JsonProperty("dataList")
	public List<String> getDataList() {
		return dataList;
	}
	
	@JsonProperty("dataList")
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}
	
	@JsonProperty("encKey")
	public String getEncKey() {
		return encKey;
	}
	
	@JsonProperty("encKey")
	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}
	
	@JsonProperty("encKCnfgData")
	public String getEncKCnfgData() {
		return encKCnfgData;
	}

	@JsonProperty("encKCnfgData")
	public void setEncKCnfgData(String encKCnfgData) {
		this.encKCnfgData = encKCnfgData;
	}
}
