package com.olympus.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KConfigurationDataRequest {
	@NotNull(message = "Please provide a keyType")
	@NotEmpty(message = "KeyType can't be empty")
	@Valid
	private String keyType;
	
	@NotNull(message = "Please provide a objectId")
	@NotEmpty(message = "ObjectId can't be empty")
	@Valid
	private String objectId;
	
//	@NotNull(message = "Please provide a derivativeKey")
//	@NotEmpty(message = "DerivativeKey can't be empty")
	@Valid
	private String derivativeKey;
	
	@NotNull(message = "Please provide a cipher")
	@Valid
	private CipherParamRequest cipher;

	@JsonProperty("cipher")
	public CipherParamRequest getCipher() {
		return cipher;
	}
	
	@JsonProperty("cipher")
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}

	@JsonProperty("keyType")
	public String getKeyType() {
		return keyType;
	}

	@JsonProperty("keyType")
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	
	@JsonProperty("objectId")
	public String getObjectId() {
		return objectId;
	}

	@JsonProperty("objectId")
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	@JsonProperty("derivativeKey")
	public String getDerivativeKey() {
		return derivativeKey;
	}

	@JsonProperty("derivativeKey")
	public void setDerivativeKey(String derivativeKey) {
		this.derivativeKey = derivativeKey;
	}

	@Override
	public String toString() {
		return "ReadConfigurationDataRequest [keyType=" + keyType + ", objectId=" + objectId + ", derivativeKey="
				+ derivativeKey + ", cipher=" + cipher + "]";
	}


}
