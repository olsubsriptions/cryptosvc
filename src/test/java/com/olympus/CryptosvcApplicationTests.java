package com.olympus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.GsonBuilder;
import com.olympus.constants.Constants;
import com.olympus.crypto.cipher.manager.CipherManager;
import com.olympus.crypto.cipher.manager.CipherManagerFactory;
import com.olympus.crypto.cipher.symm.SymmetricCipher;
import com.olympus.crypto.cipher.symm.SymmetricCipherFactory;
import com.olympus.model.CipherParamRequest;
import com.olympus.model.CnfgParams;
import com.olympus.model.CryptoRequest;
import com.olympus.model.KConfigurationDataRequest;
import com.olympus.model.KeyGeneratorRequest;
import com.olympus.repository.FirebaseConfigurationDataRepository;
import com.olympus.service.CipherService;
import com.olympus.service.ConfigurationDataService;
import com.olympus.service.KeyGeneratorService;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class CryptosvcApplicationTests {
	
	@Autowired
	KeyGeneratorService keyGenService;
	
	@Autowired
	ConfigurationDataService confDataService;
	
	@Autowired
	FirebaseConfigurationDataRepository configurationDataRepository;
	
	@Autowired
	CipherService cipherService;
	
	CnfgParams cnfgParams;
	
	
	CryptoRequest decryptDTOData;
	CryptoRequest encryptDTOData;
	KeyGeneratorRequest keyGenDTOData;
	KConfigurationDataRequest kConfDataRequest;
	
	String encryptData;
	String decryptData;
	String keyGenData;
	
	@Before
	public void init() {
		encryptData ="{\"dataList\":[\"486f6c614d756e646f\",\"4d6574656c65556e6153757363726962696461\"],\"securityKey\":\"111b701c991e586b980c8a47f3939dbf3a21c79ece72054153f1d8cdcc5af7176527b72306ef0acf7ce12b84dfdfc40cef9269a56d094aa3a9cca55c8061623a55c98f27e97c755fbfddb52199f73727ce9dcfd4eed396fbf3ffef75a792e5d4b08419899dbf061b889da3d6a891ad90\",\"cipher\":{\"algorithm\":\"AES256\",\"charset\":\"UTF-8\"}}";	
		decryptData = "{\"dataList\":[\"d4bff0c2627ecb8427caa8e4592ad442bee25064ec075dde648889e81689820c\",\"9158c5c7652fd32236894d1712ff8195980ca2f3b883e8a1ba8b7ffd0ccdefbe472740596011f0a96ba54fe126d9886a\"],\"securityKey\":\"111b701c991e586b980c8a47f3939dbf3a21c79ece72054153f1d8cdcc5af7176527b72306ef0acf7ce12b84dfdfc40cef9269a56d094aa3a9cca55c8061623a55c98f27e97c755fbfddb52199f73727ce9dcfd4eed396fbf3ffef75a792e5d4b08419899dbf061b889da3d6a891ad90\",\"cipher\":{\"algorithm\":\"AES256\",\"charset\":\"UTF-8\"}}";
		keyGenData = "{\"cipher\":{\"algorithm\":\"AES256\",\"charset\":\"UTF-8\"}}";
		
		encryptDTOData = new GsonBuilder().create().fromJson(encryptData, CryptoRequest.class);
		decryptDTOData = new GsonBuilder().create().fromJson(decryptData, CryptoRequest.class);
		
		keyGenDTOData = new GsonBuilder().create().fromJson(keyGenData, KeyGeneratorRequest.class);
		kConfDataRequest = new GsonBuilder().create().fromJson(keyGenData, KConfigurationDataRequest.class);
		
		cnfgParams = configurationDataRepository.getConfigurationData("cnfg");
	}
	
	@Test
	public void keyGen() {
		keyGenDTOData.setKeyType("SK");
		
		String securityKey = keyGenService.generateKey(keyGenDTOData);
		assertTrue(!securityKey.isEmpty());
	}
	
	@Test
	public void k1Gen() {
		kConfDataRequest.setKeyType("K1");
		kConfDataRequest.setDerivativeKey("1234567ABCBSB");
		kConfDataRequest.setObjectId("12313212312");
		
		//Generate Key
		String k1Enc = confDataService.generateConfigurationData(kConfDataRequest);
		assertTrue(!k1Enc.isEmpty());
		
		//DecryptKey
		String expectedValue = "{\"workingSubscriptionDerivedKey\":\"1234567ABCBSB\",\"subscriptionId\":\"12313212312\",\"subscriptionEndPoint\":\"services.olympus-cloud.com\",\"servicePort\":\"8080\"}";
		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance("AES256", cnfgParams.getHtpkSys(), "UTF-8");
		
		String k1Value = symKeyManager.decryptSecurityKey(k1Enc, pKeyAES);
		
		assertEquals(expectedValue, k1Value);
	}
	
//	@Test
//	public void decryptK1Gen() {
//		String k1Enc = "dc36407dee874e75c709da8b6284cb6b2c84967a792becccb061e2713c462f8e9992577ccdb0df1999bf1cafcfa8d7c88040dabbae78e97189a707a3e91a75c363132cf7f4f4142c24c051d99f14142b70dc34ad67b8d9b5a4cc152eee5c9d12e126664599c3502583a4c4afc36950dae58ee76ebba248e820961e562dc921bf5032eeabb115efedea0c079a8c0ee5a1dc2275d4e4e8f6ec754e1d9c507769037d4be4a723eef0ac1f0c8aa39a6a97b7";
//		
//		
//		CipherManager symKeyManager = CipherManagerFactory.getInstance(Constants.symmetric);
//		SymmetricCipher pKeyAES = SymmetricCipherFactory.getInstance("AES256", cnfgParams.getElement("hTPKSys"), "UTF-8");
//		String k1Value = symKeyManager.decryptSecurityKey(k1Enc, pKeyAES);
//		
//		String k1 = keyGenService.generateKey(keyGenDTOData);
//		assertTrue(!k1.isEmpty());
//	}
	
	@Test
	public void encrypt() {
		
		CipherParamRequest cipher = encryptDTOData.getCipher();
		String securityKey = encryptDTOData.getSecurityKey();
		List<String> dataList = encryptDTOData.getDataList();
		
		List<String> encryptedList = cipherService.encrypt(cipher, securityKey, dataList);
		assertEquals(encryptedList.get(0), "d4bff0c2627ecb8427caa8e4592ad442bee25064ec075dde648889e81689820c");
		assertEquals(encryptedList.get(1), "9158c5c7652fd32236894d1712ff8195980ca2f3b883e8a1ba8b7ffd0ccdefbe472740596011f0a96ba54fe126d9886a");
	}
	
	@Test
	public void decrypt() {
		CipherParamRequest cipher = decryptDTOData.getCipher();
		String securityKey = decryptDTOData.getSecurityKey();
		List<String> dataList = decryptDTOData.getDataList();
		
		List<String> decryptedList = cipherService.decrypt(cipher, securityKey, dataList);
		assertEquals(decryptedList.get(0), "486f6c614d756e646f");
		assertEquals(decryptedList.get(1), "4d6574656c65556e6153757363726962696461");
	}

}
